def countWinStreak(rounds):
    streak = 1
    i = len(rounds) - 1
    current = rounds[i]

    while i > 0:
        i -= 1
        prev = rounds[i]
        if current - prev > 2:
            break
        streak += 1
        current = prev

    return streak

def roundsSinceLastWin(rounds):
    if len(rounds) > 1:
        return rounds[-1] - rounds[-2]
    return -1
